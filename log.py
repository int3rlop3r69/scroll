import logging

def init_logger(settings):
    logger = logging.getLogger('scroll')
    logger.setLevel(settings['level'])

    formatter = logging.Formatter(settings['format'])

    if settings['console']:
        ch = logging.StreamHandler()
        ch.setFormatter(formatter)
        logger.addHandler(ch)

    fh = logging.FileHandler(settings['directory'] + '/scroll.log')
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    return logger
