
start-server:
	nohup python scroll.py >> /tmp/scroll.out 2>&1 &

stop-server:
	pkill -f scroll.py
