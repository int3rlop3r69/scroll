import asyncio
import asyncio_redis
import config
import datetime
import json
import log
import msgpack

from parse import parse

class MessageSerializer:

    def __init__(self, log_format):
        self.log_format = log_format

    def serialize(self, message, ip):
        data = parse(self.log_format, message)
        if not data:
            return

        payload = data.named
        return json.dumps(payload)

class ScrollServer:

    def __init__(self, loop, logger, config):
        self.loop = loop
        self.logger = logger
        self.keyname = config.redis['keyname']
        self.msg = MessageSerializer(config.log_format)
        self.rhost = config.redis['host']
        self.rport = config.redis['port']
        self.rdb = config.redis['db']
        asyncio.async(self.contact_redis())

    async def contact_redis(self):
        protocol_factory = lambda: asyncio_redis.RedisProtocol(db=self.rdb)
        self.rtrans, self.rconn = await self.loop.create_connection(
                                    protocol_factory, self.rhost, self.rport)

    async def enqueue(self, data):
        while True:
            try:
                await self.rconn.lpush(self.keyname, [data])
                break
            except Exception as e:
                self.logger.exception("{} - could not be pushed to redis, trying to recover.".format(data), e)
                await self.contact_redis()
                await asyncio.sleep(5)

    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        message = data.decode()
        payload = self.msg.serialize(message, addr[0])
        if payload:
            asyncio.async(self.enqueue(payload))
            self.logger.info("%s message: %s, pushed to redis." % (addr[0], message))
        else:
            self.logger.info("%s message: %s, dropped." % (addr[0], message))

    def close(self):
        self.transport.close()
        self.rtrans.close()


def main():
    addr = (config.server['host'], config.server['port'])
    logger = log.init_logger(config.logging)

    loop = asyncio.get_event_loop()
    print(datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S"))
    logger.info("Starting UDP server on %s:%s" % addr)
    scroll_server = ScrollServer(loop, logger, config)
    scroll_factory = lambda: scroll_server

    listen = loop.create_datagram_endpoint(scroll_factory, local_addr=addr)
    transport, protocol = loop.run_until_complete(listen)

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        logger.info("Shutting down...")

    scroll_server.close()
    transport.close()

main()
