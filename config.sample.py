import logging

server = {
    'host': '127.0.0.1',
    'port': 8888,
}

redis = {
    'host': 'localhost',
    'port': 6379,
    'db': 2,
    'keyname': 'event_queue',
}

logging = {
    'level': logging.DEBUG,
    'format': '%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    'console': True,
    'directory': '/tmp/',
}

log_format = '{stuff}: failed{failed_seq}'
